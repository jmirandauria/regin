export class Post {
  constructor(
    public title: string,
    public url: string,
    public author: string,
    public created_at: string,
    public id: string
  ) {}
}
