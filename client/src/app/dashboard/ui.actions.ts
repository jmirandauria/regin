import { createAction, props } from '@ngrx/store';
import { Post } from '../models/post.model';

export const isLoading = createAction('[UI Component] isLoading');
export const stopLoading = createAction('[UI Component] stopLoading');

export const setItems = createAction(
  '[Dashboard Component] Set Items',
  props<{ items: Post[] }>()
);
