import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from '../app.reducer';
import { Post } from '../models/post.model';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  constructor(
    private _postService: PostsService,
    private store: Store<AppState>
  ) {
    this._postService.getPosts();
  }

  postSubs: Subscription;
  posts: Post[] = [];
  ngOnInit(): void {
    this.postSubs = this.store
      .select('ui')
      .subscribe(({ items }) => (this.posts = items));
  }

  borrar(id: string): void {
    this._postService.borrarPost(id);
  }
  openUrl(url: string): void {
    if (url) {
      window.open(url, '_blank');
    }
  }
  ngOnDestroy() {
    this.postSubs.unsubscribe();
  }
}
