import { createReducer, on } from '@ngrx/store';
import { isLoading, stopLoading, setItems } from './ui.actions';
import { Post } from '../models/post.model';

export interface State {
  isLoading: boolean;
  items: Post[];
}

export const initialState: State = {
  isLoading: false,
  items: [],
};

const _uiReducer = createReducer(
  initialState,

  on(isLoading, (state) => ({ ...state, isLoading: true })),
  on(stopLoading, (state) => ({ ...state, isLoading: false })),
  on(setItems, (state, { items }) => ({ ...state, items: [...items] }))
);

export function uiReducer(state, action) {
  return _uiReducer(state, action);
}
