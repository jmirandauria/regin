import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as uiActions from '../dashboard/ui.actions';
import { Subscription } from 'rxjs';
import { Post } from '../models/post.model';
import { AppState } from '../app.reducer';

@Injectable({
  providedIn: 'root',
})
export class PostsService implements OnDestroy {
  apiUrl: string;
  constructor(public http: HttpClient, private store: Store<AppState>) {
    this.apiUrl = 'http://localhost:3000/api';
  }

  postSubs: Subscription;
  httpSubs: Subscription;
  httpSubsDel: Subscription;
  posts: Post[];
  newPosts: Post[];
  getPosts(): void {
    this.httpSubs = this.http.get(`${this.apiUrl}/posts/getPosts`).subscribe(
      (result: any) => {
        this.store.dispatch(uiActions.setItems({ items: result }));
      },
      (error) => {
        console.log(error);
      }
    );
  }
  borrarPost(id: string): void {
    this.httpSubsDel = this.http
      .delete(`${this.apiUrl}/posts/delete/${id}`)
      .subscribe(
        (data) => {
          this.postSubs = this.store
            .select('ui')
            .subscribe(({ items }) => (this.posts = items));
          const a = this.posts.filter((post) => post.id !== id);
          this.store.dispatch(uiActions.setItems({ items: a }));
        },
        (error) => {
          console.log(error);
        }
      );
  }

  ngOnDestroy() {
    this.postSubs.unsubscribe();
    this.httpSubs.unsubscribe();
    this.httpSubsDel.unsubscribe();
  }
}
