Repository will be separated into `server` and `client` directory for **NestJS** backend and **Angular** frontend resepctively.

- [x] Server repository
- [x] Client repository
- [x] Docker support

# clone repository

git clone https://gitlab.com/jmirandauria/regin.git

# open directory

cd regin

## Docker

Docker is supported.

- Run docker-compose up

## Server-side (NestJS)

This repository houses the Project's backend written using **NestJS**

- Run server: cd server
- npm run start:dev

## Client-side (Angular)

This repository houses the Project's frontend written using **Angular**

- Run client Angular: cd client
- ng serve
