import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostRepository } from 'src/modules/post/post.repository';

@Injectable()
export class ScheduleService {
  constructor(private postRepository: PostRepository) {}

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.postRepository.getNewPosts();
  }
}
