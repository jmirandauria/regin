import { Injectable, HttpService } from '@nestjs/common';
import { Hits } from 'src/dto/hits.dto';
import { Post } from 'src/dto/post.dto';

@Injectable()
export class FetchService {
  constructor(private http: HttpService) {}

  formatPosts(hits: Hits[], lastPostSearched?: Date): Partial<Post>[] {
    if (!Array.isArray(hits)) return [];

    let response: Partial<Post>[] = hits
      .filter(({ title, story_title }) => title || story_title)
      .map(
        ({
          title,
          story_title,
          story_url,
          author,
          created_at,
        }): Partial<Post> => ({
          title: story_title || title,
          url: story_url,
          author,
          created_at,
        }),
      );

    if (lastPostSearched) {
      response = response.filter(
        ({ created_at }) => new Date(created_at) > lastPostSearched,
      );
    }

    return response.reverse();
  }

  async fetchPosts() {
    return this.http
      .get(`http://hn.algolia.com/api/v1/search_by_date?query=nodejs`)
      .toPromise();
  }

  async getNewPosts(lastPostSearched?: Date): Promise<Partial<Post>[]> {
    try {
      const result = await this.fetchPosts();

      return this.formatPosts(result?.data?.hits || [], lastPostSearched);
    } catch (e) {
      return [];
    }
  }
}
