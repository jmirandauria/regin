import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FetchService } from 'src/services/Fetch.service';
import { Post } from './../../dto/post.dto';

@Injectable()
export class PostRepository {
  constructor(
    @InjectModel('Post') private postDb: Model<Post>,
    private algolia: FetchService,
  ) {
    this.getNewPosts();
  }

  async getPosts(): Promise<Post[]> {
    try {
      return this.postDb.find({ status: true }).sort({ created_at: -1 });
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

  async delete(id: string): Promise<void> {
    try {
      const document = await this.postDb.findOneAndUpdate(
        { _id: id },
        { status: false },
      );

      if (!document)
        throw new NotFoundException(
          `Could not find post to delete for id: ${id}`,
        );
    } catch (e) {
      if (e.status === 404) throw e;
      throw new InternalServerErrorException(e);
    }
  }

  async getLastPostSearched(): Promise<Date> {
    const lastPost = await this.postDb
      .findOne()
      .sort({ created_at: -1 })
      .select('created_at');

    return lastPost?.created_at as Date;
  }

  async insertPosts(posts: Partial<Post>): Promise<Post> {
    const post = await new this.postDb(posts).save();
    return post;
  }

  async getNewPosts(): Promise<void> {
    const lastPostSearched: Date = await this.getLastPostSearched();

    const posts: Partial<Post>[] = await this.algolia.getNewPosts(
      lastPostSearched,
    );
    posts.map(async (doc) => await this.insertPosts(doc));
  }
}
