import { Controller, Delete, Get, Param } from '@nestjs/common';
import { PostRepository } from './post.repository';
import { Post } from '../../dto/post.dto';

@Controller('posts')
export class PostController {
  constructor(private postRepository: PostRepository) {}

  @Get('/getPosts')
  getPosts(): Promise<Post[]> {
    return this.postRepository.getPosts();
  }

  @Delete('/delete/:id')
  delete(@Param('id') id: string): Promise<void> {
    return this.postRepository.delete(id);
  }
}
