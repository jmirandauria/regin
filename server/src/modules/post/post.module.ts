import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from './post.controller';
import { PostRepository } from './post.repository';
import { PostSchema } from '../../schemas/post.schema';
import { FetchService } from 'src/services/Fetch.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      {
        name: 'Post',
        schema: PostSchema,
      },
    ]),
  ],
  controllers: [PostController],
  providers: [PostRepository, FetchService],
  exports: [PostRepository],
})
export class PostModule {}
