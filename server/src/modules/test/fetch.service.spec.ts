import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { HttpService, HttpModule } from '@nestjs/common';

import { fetchObject, result } from './fake';
import { FetchService } from 'src/services/Fetch.service';

describe('FetchService', () => {
  let fetchService: FetchService;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [FetchService],
    }).compile();

    fetchService = module.get<FetchService>(FetchService);
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(fetchService).toBeDefined();
  });

  describe('fetchPosts', () => {
    it('fetches successfully data from an API', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementationOnce(() => of(fetchObject));
      await expect(fetchService.fetchPosts()).resolves.toEqual(fetchObject);
    });
  });

  describe('formatPosts', () => {
    it('should return the data when there is no last post', async () => {
      const data = fetchService.formatPosts(fetchObject.data.hits);

      expect(data).toEqual(result.reverse());
    });

    it('should return an empty array when bad params', async () => {
      const badParam: any = 'badParam';
      const data = fetchService.formatPosts(badParam);

      expect(Array.isArray(data)).toBeTruthy();
      expect(data.length).toEqual(0);
    });

    it('should return the correct result when filtered by creation date', async () => {
      const lastPostSearched = new Date('2021-1-28T14:31:54.000Z');
      const data = fetchService.formatPosts(
        fetchObject.data.hits,
        lastPostSearched,
      );

      const newResult = [...result];
      newResult.shift();

      expect(data).toEqual(newResult);
    });
  });

  describe('getNewPosts', () => {
    it('should invoke functions fetchPosts and formatPosts', async () => {
      jest.spyOn(fetchService, 'fetchPosts');
      jest.spyOn(fetchService, 'formatPosts');

      await fetchService.getNewPosts();

      expect(fetchService.fetchPosts).toBeCalled();
      expect(fetchService.fetchPosts).toBeCalledTimes(1);
      expect(fetchService.formatPosts).toBeCalled();
      expect(fetchService.formatPosts).toBeCalledTimes(1);
    });

    it('should return an empty array when there is an error in fetchPosts', async () => {
      jest.spyOn(fetchService, 'fetchPosts').mockRejectedValue(new Error());

      const data = await fetchService.getNewPosts();

      expect(Array.isArray(data)).toBeTruthy();
      expect(data.length).toEqual(0);
    });
  });
});
