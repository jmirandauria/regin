import {IsDateString, IsString, IsUrl} from 'class-validator';

export class Hits {
	@IsString()
	title: string;

	@IsString()
	story_title: string;

	@IsUrl()
	story_url: string;

	@IsString()
	author: string;

	@IsDateString()
	created_at: string;
}
